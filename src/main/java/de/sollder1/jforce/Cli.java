package de.sollder1.jforce;

import de.sollder1.jforce.dialogs.Dialog;
import de.sollder1.jforce.dialogs.Help;
import de.sollder1.jforce.dialogs.StressTest;

import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;

public class Cli {

    public static final Set<Model> DEFINED_DIALOGS = new HashSet<>();
    private static final Scanner sc = new Scanner(System.in);

    static {
        DEFINED_DIALOGS.add(new Model(Help.class, "Prints all commands and options.", "h", "help"));
        DEFINED_DIALOGS.add(new Model(StressTest.class, "Stresstest.", "s", "stress"));
    }

    public static void main(String[] args) throws Throwable {
        Cli.start();
    }

    public static void start() throws Throwable {
        var title = """
                     ██╗      ███████╗ ██████╗ ██████╗  ██████╗███████╗
                     ██║      ██╔════╝██╔═══██╗██╔══██╗██╔════╝██╔════╝
                     ██║█████╗█████╗  ██║   ██║██████╔╝██║     █████╗ \s
                ██   ██║╚════╝██╔══╝  ██║   ██║██╔══██╗██║     ██╔══╝ \s
                ╚█████╔╝      ██║     ╚██████╔╝██║  ██║╚██████╗███████╗
                 ╚════╝       ╚═╝      ╚═════╝ ╚═╝  ╚═╝ ╚═════╝╚══════╝
                """;

        println(title);
        println("You may type 'help' to get a list of commands.");
        startInterpreterLoop();
    }

    private static void startInterpreterLoop() throws Throwable {
        try {
            while (true) {
                try {
                    var command = readCommand();
                    var dialog = getDialog(command);
                    if (dialog != null) {
                        dialog.getDeclaredConstructor().newInstance().execute(command);
                    }
                } catch (ExitException ignore) {
                }
            }
        } catch (ExitException e) {
            System.exit(0);
        }
    }

    private static Class<? extends Dialog> getDialog(Command command) {
        return DEFINED_DIALOGS.stream()
                .filter(e -> e.getNames().contains(command.getCmd()))
                .findAny()
                .map(Model::getType)
                .orElse(null);
    }

    private static Command readCommand() {
        return parseCommand(read("code-gen-cli> "));
    }

    //Utils:

    public static String read(String prompt) {
        print(prompt);
        var value = sc.nextLine().trim();

        if ("exit".equals(value)) {
            throw new ExitException();
        }
        return value;
    }

    public static void print(String s, Object... args) {
        System.out.printf(s, args);
    }

    public static void println(String s, Object... args) {
        System.out.printf((s) + "%n", args);
    }

    public static Command parseCommand(String command) {
        return new Command(command);
    }

    public static String getWorkingDir() {
        return System.getProperty("user.dir");
    }

    public static class Model {
        private final Set<String> names;
        private final String description;
        private final Class<? extends Dialog> type;

        public Model(Class<? extends Dialog> type, String description, String... names) {
            this.names = Set.of(names);
            this.description = description;
            this.type = type;
        }

        public Set<String> getNames() {
            return names;
        }

        public String getDescription() {
            return description;
        }

        public Class<? extends Dialog> getType() {
            return type;
        }
    }
}
