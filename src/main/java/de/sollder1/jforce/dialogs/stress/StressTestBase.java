package de.sollder1.jforce.dialogs.stress;

import okhttp3.Call;
import okhttp3.OkHttpClient;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.TimeUnit;

public abstract class StressTestBase {

    // protected static final OkHttpClient client =

    private volatile long start;
    private volatile CyclicBarrier barrier;
    private volatile Map<String, Report> sharedReports = new HashMap<>();


    public Map<String, Report> run(Config config) throws Throwable {
        barrier = new CyclicBarrier(config.threads + 1);
        for (int i = 0; i < config.threads; i++) {
            var thread = new Thread(() -> thread(config));
            thread.setDaemon(true);
            thread.setName("Request-Daemon-" + i);
            thread.start();
        }
        start = System.nanoTime();
        barrier.await();
        return sharedReports;
    }

    private void thread(Config config) {
        try {
            var client = getClient();
            Report report = new Report();
            var now = System.nanoTime();
            while (System.nanoTime() - start < config.getRuntimeNs()) {

                var reqStart = System.nanoTime();
                var call = doRequest(client);
                try (var response = call.execute()) {
                    if (response.isSuccessful()) {
                        report.incrementSuccess();
                    } else {
                        report.incrementFailureSuccess();
                    }
                }
                report.addRequestTime(System.nanoTime() - reqStart);
                now = System.nanoTime();
            }
            report.setActualRuntime(now - start);
            sharedReports.put(Thread.currentThread().getName(), report);
            barrier.await();
        } catch (InterruptedException | BrokenBarrierException | IOException e) {
            throw new RuntimeException(e);
        }
    }


    public abstract Call doRequest(OkHttpClient client) throws IOException;

    public abstract String name();


    private OkHttpClient getClient() {
        return new OkHttpClient.Builder()
                .connectTimeout(5, TimeUnit.SECONDS)
                .build();
    }

    public static class Report {
        private int requestsSucceeded;
        private int requestsFailed;

        private Long actualRuntime;
        private final List<Long> requestTimes = new ArrayList<>();


        public int getRequestsSucceeded() {
            return requestsSucceeded;
        }

        public void setRequestsSucceeded(int requestsSucceeded) {
            this.requestsSucceeded = requestsSucceeded;
        }

        public int getRequestsFailed() {
            return requestsFailed;
        }

        public void setRequestsFailed(int requestsFailed) {
            this.requestsFailed = requestsFailed;
        }

        public Long getActualRuntime() {
            return actualRuntime;
        }

        public void setActualRuntime(Long actualRuntime) {
            this.actualRuntime = actualRuntime;
        }

        public void incrementSuccess() {
            requestsSucceeded++;
        }

        public void incrementFailureSuccess() {
            requestsFailed++;
        }

        public List<Long> getRequestTimes() {
            return requestTimes;
        }

        public void addRequestTime(Long requestTime) {
            this.requestTimes.add(requestTime);
        }

        @Override
        public String toString() {
            return "Report{" +
                    "requestsSucceeded=" + requestsSucceeded +
                    ", requestsFailed=" + requestsFailed +
                    ", actualRuntime=" + actualRuntime +
                    ", requestTimes=" + requestTimes +
                    '}';
        }
    }

    public static class Config {
        private int threads;
        private long runtimeNs;

        public int getThreads() {
            return threads;
        }

        public void setThreads(int threads) {
            this.threads = threads;
        }

        public long getRuntimeNs() {
            return runtimeNs;
        }

        public void setRuntimeNs(long runtimeNs) {
            this.runtimeNs = runtimeNs;
        }

        @Override
        public String toString() {
            return "Config{" +
                    "threads=" + threads +
                    ", runtimeNs=" + runtimeNs +
                    '}';
        }
    }

}
