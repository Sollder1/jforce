package de.sollder1.jforce.dialogs.stress;

import okhttp3.Call;
import okhttp3.OkHttpClient;
import okhttp3.Request;

import java.io.IOException;

public class SimpleEndpointWithLogging extends StressTestBase {


    @Override
    public Call doRequest(OkHttpClient client) throws IOException {
        Request request = new Request.Builder()
                .url("https://900m7.backend.sollder1.de/yfm/rest/budget-book?query=&from=2022-11-01&to=2022-11-30&categoryId=ALL")
                .header("Authorization", "693bcfc5-9d95-47c5-8e5c-173c4336f383")
                .get()
                .build();
        return client.newCall(request);
    }

    @Override
    public String name() {
        return "Endpoint no logging";
    }
}
