package de.sollder1.jforce.dialogs;

import de.sollder1.jforce.Cli;
import de.sollder1.jforce.Command;
import de.sollder1.jforce.dialogs.stress.SimpleEndpointWithLogging;
import de.sollder1.jforce.dialogs.stress.StressTestBase;
import org.jetbrains.annotations.NotNull;

import java.util.List;
import java.util.Map;

public class StressTest extends Dialog {

    private static final List<? extends StressTestBase> impls = List.of(new SimpleEndpointWithLogging());

    @Override
    public void execute(Command command) throws Throwable {
        Cli.println("Available Tests: ");
        impls.forEach(stressTestBase -> Cli.println("\t- %s ".formatted(stressTestBase.name())));
        var index = Integer.parseInt(Cli.read("Choose one or paste config: "));
        var reports = impls.get(index - 1).run(getConfig());
        report(reports);
    }

    @NotNull
    private static StressTestBase.Config getConfig() {
        var config = new StressTestBase.Config();
        config.setThreads(16);
        config.setRuntimeNs((long) (1E9 * 5));
        Cli.println("Config: %s", config);
        return config;
    }

    private void report(Map<String, StressTestBase.Report> reports) {
        Cli.println("########## Report ##########");
        reportTotals(reports);
        reportRuntimeAritmeticAverage(reports);
    }

    private void reportTotals(Map<String, StressTestBase.Report> reports) {
        var s = reports.values().stream().map(StressTestBase.Report::getRequestsSucceeded).reduce(Math::addExact).orElse(0);
        var f = reports.values().stream().map(StressTestBase.Report::getRequestsFailed).reduce(Math::addExact).orElse(0);
        Cli.println("Successful-Requests: %s", s);
        Cli.println("Failed-Requests: %s", f);
    }

    private void reportRuntimeAritmeticAverage(Map<String, StressTestBase.Report> reports) {
        var avg = reports.values().stream().flatMap(report -> report.getRequestTimes().stream()).mapToLong(Long::longValue).average().orElse(0);
        Cli.println("Average-Request-Time: %s ms", avg / 1E6);

    }
}
