package de.sollder1.jforce.dialogs;


import de.sollder1.jforce.Cli;
import de.sollder1.jforce.Command;

public class Help extends Dialog {

    @Override
    public void execute(Command command) {
        Cli.println("Available commands: ");
        Cli.DEFINED_DIALOGS.forEach(this::print);
        Cli.println("\t- exit");
    }

    private void print(Cli.Model model) {
        String names = String.join(" | ", model.getNames());
        Cli.println("\t- %s -> %s", names, model.getDescription());
    }


}
