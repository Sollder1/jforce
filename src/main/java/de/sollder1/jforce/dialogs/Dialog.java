package de.sollder1.jforce.dialogs;

import de.sollder1.jforce.Command;

public abstract class Dialog {

    public abstract void execute(Command command) throws Throwable;



}
