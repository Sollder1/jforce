package de.sollder1.jforce;

import java.util.HashMap;
import java.util.Map;

public class Command {
    private final String cmd;
    private final Map<String, String> options;

    public Command(String cmd) {
        this.cmd = cmd;
        options = new HashMap<>();
    }

    public Command(String cmd, Map<String, String> options) {
        this.cmd = cmd;
        this.options = options;
    }

    public String getCmd() {
        return cmd;
    }

    public Map<String, String> getOptions() {
        return options;
    }
}
