package de.sollder1.jforce.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import de.sollder1.jforce.Cli;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Optional;

public final class ConfigApi {
    private static final String FILE_NAME = "code-gen-cli-config.json";

    private static final ObjectMapper MAPPER = new ObjectMapper();

    private ConfigApi() {

    }

    public static Optional<ConfigModel> getConfig() throws IOException {
        var path = getPath(Cli.getWorkingDir());

        if (Files.exists(path)) {
            return Optional.of(MAPPER.readValue(path.toFile(), ConfigModel.class));
        }
        return Optional.empty();
    }

    public static void writeConfig(ConfigModel config) throws IOException {
        var path = getPath(config.getProjectDir());
        Cli.println("Writing config to: '%s'", path.toString());
        MAPPER.writeValue(path.toFile(), config);
    }

    private static Path getPath(String path) {
        return Paths.get(path, FILE_NAME);
    }

}
