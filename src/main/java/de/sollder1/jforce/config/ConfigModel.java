package de.sollder1.jforce.config;

public class ConfigModel {
    private String projectDir;
    private String processorPackage;
    private String modelPackage;
    private String endpointPackage;

    public String getProjectDir() {
        return projectDir;
    }

    public void setProjectDir(String projectDir) {
        this.projectDir = projectDir;
    }

    public String getProcessorPackage() {
        return processorPackage;
    }

    public void setProcessorPackage(String processorPackage) {
        this.processorPackage = processorPackage;
    }

    public String getModelPackage() {
        return modelPackage;
    }

    public void setModelPackage(String modelPackage) {
        this.modelPackage = modelPackage;
    }

    public String getEndpointPackage() {
        return endpointPackage;
    }

    public void setEndpointPackage(String endpointPackage) {
        this.endpointPackage = endpointPackage;
    }
}
